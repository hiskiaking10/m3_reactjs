export default {
    items: [
        {
            id: 'home',
            title: 'HOME',
            type: 'group',
            icon: 'icon-navigation',
            children: [
                {
                    id: 'dashboard',
                    title: 'Dashboard',
                    type: 'item',
                    url: '/dashboard/default',
                    icon: 'feather icon-home',
                }
            ]
        },
        {
            id: 'manage',
            title: 'MANAGE',
            type: 'group',
            icon: 'icon-group',
            children: [
                {
                    id: 'profile',
                    title: 'Profile',
                    type: 'item',
                    url: '/manage/profile',
                    icon: 'feather icon-file-text'
                },
                {
                    id: 'user',
                    title: 'User',
                    type: 'item',
                    icon: 'feather icon-server',
                    url: '/manage/users'
                }
            ]
        },
        {
            id: 'billing',
            title: 'BILLING',
            type: 'group',
            icon: 'icon-charts',
            children: [
                {
                    id: 'overview',
                    title: 'Overview',
                    type: 'item',
                    icon: 'feather icon-pie-chart',
                    url: '/billing/overview'
                },
                {
                    id: 'invoice',
                    title: 'Invoice',
                    type: 'item',
                    icon: 'feather icon-map',
                    url: '/billing/invoice'
                }
            ]
        },
        {
            id: 'settings',
            title: 'SETTINGS',
            type: 'group',
            icon: 'icon-charts',
            children: [
                {
                    id: 'menu',
                    title: 'Menu',
                    type: 'item',
                    icon: 'feather icon-box',
                    url: '/setting/menus'
                },
                {
                    id: 'role',
                    title: 'Role',
                    type: 'item',
                    icon: 'feather icon-map',
                    url: '/setting/roles'
                }
            ]
        },
        
    ]
}