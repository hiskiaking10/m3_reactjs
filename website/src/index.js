import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { ScrollContext } from 'react-router-scroll-4';

import store from './store';
import Login from './components/page/login';
import Signup from './components/page/signup';
import App from './components/app';
import FormMenu from './components/page/form_menu';
// import Layout from '../src/layout';
// import MyLayout from './test/mylayout';
import Loader from './components/layout/Loader'
import Aux from "./hoc/_Aux";
import ScrollToTop from './components/layout/ScrollToTop';
import * as serviceWorker from './serviceWorker';
import config from './config';



class Root extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<BrowserRouter basename={'/'}>
					{/* <ScrollContext> */}
					<ScrollToTop>
						<Suspense fallback={<Loader />}>
							<Switch>
								<Route exact path={`${process.env.PUBLIC_URL}/`} component={Login} />
								<Route exact path={`${process.env.PUBLIC_URL}/signup`} component={Signup} />
								<App>
									{/* <Route exact path={`${process.env.PUBLIC_URL}/home`} component={Layout} /> */}
									{/* <Route exact path={`${process.env.PUBLIC_URL}/mylayout`} component={MyLayout} /> */}
									{/* <Route exact path={`${process.env.PUBLIC_URL}/setting/menus`} component={FormMenu} /> */}
								</App>
							</Switch>
						</Suspense>
					</ScrollToTop>
					{/* </ScrollContext> */}
				</BrowserRouter>
			</Provider>
		)
	}
}
ReactDOM.render(<Root />, document.getElementById('root'));
serviceWorker.unregister();
