import React, { Component } from 'react';
import * as Cookies from "js-cookie";

class Variable {
    
    URLAPI=()=>{
        return 'http://api.stecindo.com/api/';
    }

    isLogin=()=>{
        const sessionCookie = Cookies.get("isLogin");
        if (sessionCookie === undefined) {
            return false;
        } else {
            return true;
        }
    }

    setLogin=()=>{
        Cookies.remove("isLogin");
        Cookies.set("isLogin", true, {  });
    }

    setToken=(token)=>{
        Cookies.remove("token");
        Cookies.set("token", token, {  });
    }

    getToken=()=>{
        const sessionCookie = Cookies.get("token");
        if (sessionCookie === undefined) {
            return "";
        } else {
            return sessionCookie;
        }
    }

    signOut=()=>{
        Cookies.remove("isLogin");
        Cookies.remove("token");
    }
}
const mVariable = new Variable();
export default mVariable;