import React, { Component, useState } from 'react';
// import 'semantic-ui-css/semantic.min.css';

import variable from '../../../helper/variable';
import "./form_menu.css";

class FormMenu extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {

        var token = variable.getToken();
        console.log('componentDidMount - token : ' + token);

        console.log('componentDidMount');
        fetch("http://localhost:3000/api/Menu?access_token=" + token)
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result)
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    filterMenu(props) {

        return (this.state.items.map((value, key) =>
            <div class="item">
                <i class="circle icon"></i>
                <div class="content">
                    <div class="header">{value.M_Label}</div>
                    <div class="description">{value.M_Name}</div>
                </div>
            </div>
        ))
    }

    render() {
        return (
            <div class="menu_frame">
                <div class="html ui top attached segment">
                    <div class="ui list">
                        {this.filterMenu}
                        <div class="item">
                            <i class="circle icon"></i>
                            <div class="content">
                                <div class="header">src</div>
                                <div class="description">Source files for project</div>
                                <div class="list">
                                    <div class="item">
                                        <i class="angle double right icon"></i>
                                        <div class="content">
                                            <div class="header">site</div>
                                            <div class="description">Your site's theme</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="folder icon"></i>
                                        <div class="content">
                                            <div class="header">themes</div>
                                            <div class="description">Packaged theme files</div>
                                            <div class="list">
                                                <div class="item">
                                                    <i class="folder icon"></i>
                                                    <div class="content">
                                                        <div class="header">default</div>
                                                        <div class="description">Default packaged theme</div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <i class="folder icon"></i>
                                                    <div class="content">
                                                        <div class="header">my_theme</div>
                                                        <div class="description">Packaged themes are also available in this folder</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="file icon"></i>
                                        <div class="content">
                                            <div class="header">theme.config</div>
                                            <div class="description">Config file for setting packaged themes</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <i class="circle icon"></i>
                            <div class="content">
                                <div class="header">dist</div>
                                <div class="description">Compiled CSS and JS files</div>
                                <div class="list">
                                    <div class="item">
                                        <i class="folder icon"></i>
                                        <div class="content">
                                            <div class="header">components</div>
                                            <div class="description">Individual component CSS and JS</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <i class="circle icon"></i>
                            <div class="content">
                                <div class="header">semantic.json</div>
                                <div class="description">Contains build settings for gulp</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default FormMenu;

