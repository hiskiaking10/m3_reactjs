import React, { Component, useState } from 'react';
import { NavLink } from 'react-router-dom';
import variable from '../../helper/variable';
// import '../../assets/css/App.css';
// import '../../../node_modules/font-awesome/scss/font-awesome.scss';
import '../../assets/scss/style.scss';

class Signup extends Component {


    register = () => {

        console.log('componentDidMount');
        fetch("http://localhost:3000/api/User/login", {
            method: 'POST',
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                username: "superadmin",
                password: "password"
            }),
        }).then(res => res.json())
            .then(
                (result) => {
                    console.log(result);
                    console.log('token: ' + result.id);
                    variable.setToken(result.id);
                    document.location.href = "settings/form_menu";
                    // this.setState({
                    //     isLoaded: true,
                    //     items: result.items
                    // });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }


    render() {
        return (
            <div className="auth-wrapper">
                <div className="auth-content">
                    <div className="auth-bg">
                        <span className="r" />
                        <span className="r s" />
                        <span className="r s" />
                        <span className="r" />
                    </div>
                    <div className="card">
                        <div className="card-body text-center">
                            <div className="mb-4">
                                <i className="feather icon-user-plus auth-icon" />
                            </div>
                            <h3 className="mb-4">Sign up</h3>
                            <div className="input-group mb-3">
                                <input type="text" className="form-control" placeholder="Username" />
                            </div>
                            <div className="input-group mb-3">
                                <input type="email" className="form-control" placeholder="Email" />
                            </div>
                            <div className="input-group mb-4">
                                <input type="password" className="form-control" placeholder="Password" />
                            </div>
                            {/* <div className="form-group text-left">
                                <div className="checkbox checkbox-fill d-inline">
                                    <input type="checkbox" name="checkbox-fill-2" id="checkbox-fill-2" />
                                    <label htmlFor="checkbox-fill-2" className="cr">Send me the <a href={'#!'}> Newsletter</a> weekly.</label>
                                </div>
                            </div> */}
                            <button className="btn btn-primary shadow-2 mb-4">Sign up</button>
                            <p className="mb-0 text-muted">Allready have an account? <NavLink to="/">Login</NavLink></p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Signup;

