import React, { Component, useState } from 'react';
import { NavLink } from 'react-router-dom';
// import 'semantic-ui-css/semantic.min.css';
// import '../../assets/css/login.css';
import variable from '../../helper/variable';
// import '../../assets/css/App.css';
// import '../../../node_modules/font-awesome/scss/font-awesome.scss';
import '../../assets/scss/style.scss';

class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: "",
            isSaveChecked: "0"
        };
    }

    login = () => {

        console.log('componentDidMount');
        fetch("http://localhost:3000/api/User/login", {
            method: 'POST',
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password
            }),
        }).then(res => res.json())
            .then(
                (result) => {
                    console.log(result);
                    console.log('token: ' + result.id);
                    variable.setToken(result.id);
                    document.location.href = "/home";
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    handleEmail = (e) => {
        this.setState({
            username: e.target.value
        });
    }

    handlePassword = (e) => {
        this.setState({
            password: e.target.value
        });
    }

    render() {
        return (
            <div className="auth-wrapper">
                <div className="auth-content">
                    <div className="auth-bg">
                        <span className="r" />
                        <span className="r s" />
                        <span className="r s" />
                        <span className="r" />

                    </div>
                    <div className="card">
                        <div className="card-body text-center">
                            <div className="mb-4">
                                <i className="feather icon-unlock auth-icon" />
                            </div>
                            <h3 className="mb-4">Login</h3>
                            <div className="input-group mb-3">
                                <input type="email" className="form-control" placeholder="Email" onChange={this.handleEmail}/>
                            </div>
                            <div className="input-group mb-4">
                                <input type="password" className="form-control" placeholder="password" onChange={this.handlePassword}/>
                            </div>
                            <div className="form-group text-left">
                                <div className="checkbox checkbox-fill d-inline">
                                    <input type="checkbox" name="checkbox-fill-1" id="checkbox-fill-a1" />
                                    <label htmlFor="checkbox-fill-a1" className="cr"> Save credentials</label>
                                </div>
                            </div>
                            <button className="btn btn-primary shadow-2 mb-4" onClick={this.login}>Login</button>
                            <p className="mb-2 text-muted">Forgot password? <NavLink to="/">Reset</NavLink></p>
                            <p className="mb-0 text-muted">Don’t have an account? <NavLink to="/signup">Signup</NavLink></p>
                        </div>
                    </div>
                </div>
            </div>
            // </div>
        );
    }
}
export default Login;

