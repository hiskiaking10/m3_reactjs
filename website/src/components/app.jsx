import React, { Component, Suspense } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import windowSize from 'react-window-size';

import Navigation from './layout/AdminLayout/Navigation';
import NavBar from './layout/AdminLayout/NavBar';
import routes from "../routes";
import Breadcrumb from './layout/AdminLayout/Breadcrumb';
import Aux from "../hoc/_Aux";
import * as actionTypes from "../store/actions";
import Loader from "./layout/Loader";

class App extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        if (this.props.windowWidth > 992 && this.props.windowWidth <= 1024 && this.props.layout !== 'horizontal') {
            this.props.onComponentWillMount();
        }
    }

    mobileOutClickHandler() {
        if (this.props.windowWidth < 992 && this.props.collapseMenu) {
            this.props.onComponentWillMount();
        }
    }
    render() {
        /* full screen exit call */
        document.addEventListener('fullscreenchange', this.fullScreenExitHandler);
        document.addEventListener('webkitfullscreenchange', this.fullScreenExitHandler);
        document.addEventListener('mozfullscreenchange', this.fullScreenExitHandler);
        document.addEventListener('MSFullscreenChange', this.fullScreenExitHandler);

        const menu = routes.map((route, index) => {
            return (route.component) ? (
                <Route
                    key={index}
                    path={route.path}
                    exact={route.exact}
                    name={route.name}
                    render={props => (
                        <route.component {...props} />
                    )} />
            ) : (null);
        });

        return (
            <Aux>
                {/* <div> */}
                <Navigation />
                <NavBar />
                <div className="pcoded-main-container" onClick={() => this.mobileOutClickHandler}>
                    <div className="pcoded-wrapper">
                        <div className="pcoded-content">
                            <div className="pcoded-inner-content">
                                <Breadcrumb />
                                <div className="main-body">
                                    <div className="page-wrapper">
                                        <Suspense fallback={<Loader />}>
                                            <Switch>
                                                {menu}
                                                <Redirect from="/" to={this.props.defaultPath} />
                                            </Switch>
                                        </Suspense>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    {this.props.children}
                </div>
                <div>INI FOOTER</div>
                {/* </div> */}
            </Aux>

            // <div>
            //     <div>INI HEADER</div>
            //     <Aux>
            //         <ScrollToTop>
            //             <Suspense fallback={<Loader />}>
            //                 <Switch>
            //                     <Route path="/home" component={AdminLayout} />
            //                 </Switch>
            //             </Suspense>
            //         </ScrollToTop>
            //     </Aux>
            //     <div>INI FOOTER</div>
            // </div>
        );
    }


}
// export default App;

const mapStateToProps = state => {
    return {
        defaultPath: state.defaultPath,
        isFullScreen: state.isFullScreen,
        collapseMenu: state.collapseMenu,
        configBlock: state.configBlock,
        layout: state.layout
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFullScreenExit: () => dispatch({ type: actionTypes.FULL_SCREEN_EXIT }),
        onComponentWillMount: () => dispatch({ type: actionTypes.COLLAPSE_MENU })
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(windowSize(App));